function tinhLuong(){
    var luongMotNgay = document.getElementById("txt-luong-1-ngay").value*1;
    var soNgayLam = document.getElementById("txt-so-ngay-lam").value*1;
    var result1 = luongMotNgay*soNgayLam;
    var result1El = document.getElementById("txt_tong_luong").setAttribute('value', new Intl.NumberFormat('vn-VN', {style: 'currency', currency: "VND"}).format(result1));
}

function tinhTrungBinh(){
    var so1 = document.getElementById("txt-so-thu-1").value*1;
    var so2 = document.getElementById("txt-so-thu-2").value*1;
    var so3 = document.getElementById("txt-so-thu-3").value*1;
    var so4 = document.getElementById("txt-so-thu-4").value*1;
    var so5 = document.getElementById("txt-so-thu-5").value*1;
    var result2 = (so1+so2+so3+so4+so5)/5;
    var result2El = document.getElementById("txt_trung_binh").setAttribute('value', result2);
}

function tinhQuyDoi(){
    var usd = 23500;
    var soTien = document.getElementById("txt-so-tien-usd").value*1;
    var result3 = usd*soTien;
    var result3El = document.getElementById("txt-quy-doi").setAttribute('value', new Intl.NumberFormat('vn-VN', {style: 'currency', currency: "VND"}).format(result3));
}

function tinhHCN(){
    var chieuDai = document.getElementById("txt-chieu-dai").value*1;
    var chieuRong = document.getElementById("txt-chieu-rong").value*1;
    var result4 = chieuDai*chieuRong;
    var result5 = (chieuDai+chieuRong)*2;
    var result4El = document.getElementById("txt_tinh_hcn").setAttribute('value',`Diện tích: ${result4}; Chu vi: ${result5}`);
}

function tinhTong(){
    var soHaiChuSo = document.getElementById("txt-2-chu-so").value*1;
    var chuc = Math.floor(soHaiChuSo/10);
    var donvi = soHaiChuSo%10;
    var result6 = chuc+donvi;
    var result6El = document.getElementById("txt-tinh-tong").setAttribute('value', result6);
}


function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
}

function bai3(){
    document.getElementById("e3").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
}

function bai4(){
    document.getElementById("e4").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e5").style.display = "none";
}

function bai5(){
    document.getElementById("e5").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
}
